var express = require('express')
var app = express()

var PORT = process.env.PORT || 8080;
var USER = process.env.USER || "TEST USER";

app.get('/', function(req, res) {
	res.send('Hello, Norhan!' + process.env.USER);
});

app.listen(PORT);
console.log("Started on localhost:" + PORT);
